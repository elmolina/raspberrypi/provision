#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import configparser
import subprocess

config_ini = "eventtv.ini"
config_parser = configparser.ConfigParser()
config_parser.read(config_ini)                         # read config file and store its values in config_parser variable 

mqtt_hostname = config_parser.get("MQTT", "host")    
mqtt_topic = config_parser.get("MQTT", "topic")

def start_service(serviceName):
    stat = subprocess.call(["systemctl", "start", "--quiet", serviceName])
    if(stat == 0):  # if 0 (active), print "Active"
        print("Active")

def stop_service(serviceName):
    stat = subprocess.call(["systemctl", "stop", "--quiet", serviceName])
    if(stat == 0):  # if 0 (active), print "Active"
        print("Active")

def initMQTT():
    mqttClient = mqtt.Client()
    try:                 
        print("Connecting to host: " + mqtt_hostname)
        # Starts a new thread, that calls the loop method at regular intervals for you. It also handles re-connects automatically.
        mqttClient.loop_start()                        # Non blocking method. We want to manage the fan though no MQTT server is present              
        mqttClient.connect(mqtt_hostname)
        return mqttClient
    except:                                            # Error control. The program will continue though there is no MQTT server
        return mqttClient

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code: '"+str(rc) + "'")
    
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(mqtt_topic)
    print("Subscribed to topic: '" + mqtt_topic + "'")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    payload = msg.payload.decode('UTF-8')
    print("Recieved from topic: '" + msg.topic +"' payload: '" + payload + "'")
    if (payload == "idle"):
        start_service("acestream")
    elif (payload == "unavailable"):
        stop_service("acestream")

print ("Loading configuration OK")
mqttClient = initMQTT()                            # init MQTT client with config parameters
mqttClient.on_connect = on_connect
mqttClient.on_message = on_message

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
mqttClient.loop_forever()