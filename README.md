# Provisioning a Raspberry Pi 4

Raúl Molina García, November 2019

## 1. Introduction
This ansible script MUST be executed after following the instrucions located in [securize](https://gitlab.com/elmolina/raspberrypi/securize) project 

## 2. Details
This ansible script installs and configure this software:
- Plex server
- Transmission
- USB-drive
- Installs the torrent scripts located in this [project](https://gitlab.com/elmolina/autotorrent)

## 3. Execute the playbook
```
cd provision
sudo ansible-playbook local.yml
```

## 5. References

https://github.com/glennklockwood/rpi-ansible